#!/usr/bin/env python
from pyparsing import *
import re
import string

# A high level description of the Simulink mdl file format
SIMULINK_BNF = """
object {
     members
}
members
    variablename  value
    object {
        members
    }
variablename

array
    [ elements ]
matrix
    [elements ; elements]
elements
    value
    elements , value
value
    string
    doublequotedstring
    float
    integer
    object
    array
    matrix
"""

# parse actions
def convertNumbers(s,l,toks):
    """Convert tokens to int or float"""
    # Taken from jsonParser.py
    n = toks[0]
    try:
        return int(n)
    except ValueError as ve:
        return float(n)

def joinStrings(s,l,toks):
    """Join string split over multiple lines"""
    return ["".join(toks)]


def mdlParser(mdlFilePath):
    mdlData = open(mdlFilePath,'r').read()
    # Define grammar

    # Parse double quoted strings.  Ideally we should have used the simple
    # statement:
    #    dblString = dblQuotedString.setParseAction( removeQuotes )
    # Unfortunately dblQuotedString does not handle special chars like \n \t,
    # so we have to use a custom regex instead.
    # See http://pyparsing.wikispaces.com/message/view/home/3778969 for
    # details.
    dblString = Regex(r'\"(?:\\\"|\\\\|[^"])*\"', re.MULTILINE)
    dblString.setParseAction(removeQuotes)
    mdlNumber = Combine(Optional('-') + ('0' | Word('123456789',nums)) + Optional('.' + Word(nums)) + Optional(Word('eE',exact=1) + Word(nums + '+-',nums)))
    mdlObject = Forward()
    mdlName = Word('$' + '.' + '_' + alphas + nums)
    mdlValue = Forward()
    # Strings can be split over multiple lines
    mdlString = (dblString + Optional(OneOrMore(Suppress(LineEnd()) + LineStart() + dblString)))
    mdlElements = delimitedList(mdlValue)
    mdlArray = Group(Suppress('[') + Optional(mdlElements) + Suppress(']'))
    mdlMatrix = Group(Suppress('[') + (delimitedList(Group(mdlElements),';')) \
                  + Suppress(']'))
    # to match case such as '[0U]'
    mdlExtra = Group(Suppress('[') + Word(alphanums) + Suppress(']'))
    #mdlValue << (mdlNumber | mdlName | mdlString | mdlArray | mdlMatrix)
    mdlValue << (mdlNumber | mdlName | mdlString | mdlArray | mdlMatrix | mdlExtra)
    memberDef = Group(mdlName + mdlValue) | Group(mdlObject)
    mdlMembers = OneOrMore(memberDef)
    mdlObject << (mdlName + Suppress('{') + Optional(mdlMembers) + Suppress('}'))
    mdlNumber.setParseAction(convertNumbers)
    mdlString.setParseAction(joinStrings)
    # Some mdl files from Mathworks start with a comment.  Ignore all
    # lines that start with a #
    singleLineComment = Group("#" + restOfLine)
    mdlObject.ignore(singleLineComment)
    mdlParser = mdlObject
    result = mdlParser.parseString(mdlData)
    return result

# The configuration file includes the library describing the Verilog modules,
# and the file that defines the mapping rules between the mdl blocks and the
# Verilog modules.  The grammar of the configuration file is the same as the
# Simulink mdl file except it allows multiple top level objects defined rather
# than only one top module in the file.
def configurationParser(cfgFilePath):
    """
    This function parses the configuration files.

    The configuration files include the library file and the mapping file which have
    the similar grammar as the Simulink mdl file.
    """

    cfgData = open(cfgFilePath,'r').read()
    
    dblString = Regex(r'\"(?:\\\"|\\\\|[^"])*\"', re.MULTILINE)
    dblString.setParseAction(removeQuotes)
    cfgNumber = Combine(Optional('-') + ('0' | Word('123456789',nums)) + 
                        Optional('.' + Word(nums)) + Optional(Word('eE',exact=1) 
                        + Word(nums + '+-',nums)))
    cfgObject = Forward()
    cfgName = Word('$' + '.' + '_' + alphas + nums)
    cfgValue = Forward()
    # Strings can be split over multiple lines
    cfgString = (dblString + Optional(OneOrMore(Suppress(LineEnd()) + LineStart() + dblString)))
    cfgElements = delimitedList(cfgValue)
    cfgArray = Group(Suppress('[') + Optional(cfgElements) + Suppress(']'))
    cfgMatrix = Group(Suppress('[') + (delimitedList(Group(cfgElements),';')) \
                  + Suppress(']'))
    cfgValue << (cfgNumber | cfgName | cfgString | cfgArray | cfgMatrix)
    memberDef = Group(cfgName + cfgValue) | Group(cfgObject)
    cfgMembers = OneOrMore(memberDef)
    cfgObject << (cfgName + Suppress('{') + Optional(cfgMembers) + Suppress('}'))
    cfgNumber.setParseAction(convertNumbers)
    cfgString.setParseAction(joinStrings)
    # Some mdl files from Mathworks start with a comment.  Ignore all
    # lines that start with a "#"
    singleLineComment = Group("#" + restOfLine)
    cfgObject.ignore(singleLineComment)
    cfgParser = OneOrMore(Group(cfgObject))
    result = cfgParser.parseString(cfgData)
    return result