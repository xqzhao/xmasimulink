#!/usr/bin/env python
# Filename: mdl2vlog.py
# Function: this file is the command interface to transfer the mdl file into Verilog-HDL codes

from xMAS2Verilog import SimulinkToVerilog as sv
import argparse
import os

def main():
    # the commandline parser
    cmdParser = argparse.ArgumentParser(description="GENERATE THE VERILOG-HDL IMPLEMENTATION FROM THE xMAS MODEL IN Simulink")
    cmdParser.add_argument('-l', '--vlib', action="store", dest="vlibFile", default="xmas_verilog.vlib", help="set the Verilog HDL library description file")
    cmdParser.add_argument('-r', '--rule', action="store", dest="mapFile", default="xmas_vlog_mdl.map", help="set the mapping rule description file")
    cmdParser.add_argument('xMASModel', action="store", help="set the Simulink model file, which is to be transferred into Verilog implementation")
    group = cmdParser.add_mutually_exclusive_group()
    group.add_argument('-m', '--isModule', action="store_true", default=True, help="indicate that the generated Verilog file is a module")
    group.add_argument('-t', '--isTestbench', action="store_true", help="indicate that the generated Verilog file is a testbench")
    cmdParser.add_argument('-c', '--mlib', action="store_true", help="indicate that the corresponding vlib description is generated for the destination Verilog module")
#    cmdParser.add_argument('-n', '--rename', action="store", dest="moduleName", help="rename the generated Verilog module")
    cmdParser.add_argument('-v', '--version', action="version", version="%(prog)s 1.0 release")
    args = cmdParser.parse_args()

    verilogFile = sv()

    if os.path.isfile(args.vlibFile):
        verilogFile.libFilePath = args.vlibFile
    else:
        print("The libfile is not valid!\n")
        exit(0)

    if os.path.isfile(args.mapFile):
        verilogFile.cfgFilePath = args.mapFile
    else:
        print("The mapping file is not valid!\n")
        exit(0)

    if os.path.isfile(args.xMASModel):
        verilogFile.mdlFilePath = args.xMASModel
        verilogFile.targetDirectory = os.path.dirname(args.xMASModel)
    else:
        print("The mdl file is not valid!\n")
        exit(0)

    #if args.moduleName:    # the module is renamed
    #   verilogFile.systemName = args.moduleName

    if args.isTestbench:
        args.isModule = False

    if args.isModule:
        if args.mlib:
            verilogFile.generateLib = args.mlib
        verilogFile.generateSystem()

    if args.isTestbench:
        verilogFile.printTestbench()

if __name__ == '__main__':
    main()