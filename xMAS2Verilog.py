#!/usr/bin/env python
# Filename: xMAS2Verilog.py

from parseTool import mdlParser, configurationParser
import os
import copy

TAB_SPACE_RATIO = 4     # the ratio between tab and space to help format the text

# The function to get the list that describes the system, including blocks and
# lines
def getSectionList(mdlList, sectionName):
    """
    This function extract the sub-list from the mdl list according to the section name.

    The sublist is obtained by searching the section name in the mdl list. For example,
    it obtains the system blocks and lines by specifying the section name as "System" when
    searching from the parsed mdl file list.
    Varialbes:
        mdlList: the input list structure;
        sectionName: the key work to be searched to slice the mdlList.
    """
    for section in mdlList:     # each section of input list
        if section[0] == sectionName:
            if len(section) < 2:
                assert('The sublist of ' + sectionName + 'is none!')
                return None
            else:
                return section   # the returned value can be a list or a single value
        else:
             assert('The section of ' + sectionName + 'is not found!')

# Define the class to represent the parameter in Verilog HDL
class VerilogParameter:

    def __init__(self, parameterName = '',
                 parameterType = 'integer',
                 parameterValue = 0):
        """
        The construction function of a parameter.

        Variables:
            parameterName: the parameter name defined in the Verilog module;
            parameterInstanceName: the instantiation name of the parameter;
            parameterType: the parameter type, e.g. 'integer', 'real', 'string'
            parameterValue: the parameter value
            defaultValue: default value of the parameter
        """
        self.parameterName = parameterName
        self.parameterInstanceName = parameterName
        self.parameterType = parameterType
        self.parameterValue = parameterValue
        self.defaultValue = parameterValue

# Define the class to represent the channel in xMAS
class VerilogPort:

    def __init__(self, portID = 1,
                 portType = 'outport',
                 portName = '',
                 portInputReady = '',
                 portData = '',
                 portOutputReady = ''):
        """
        The construction function of a parameter.

        Variables:
            portID: the ID of the port;
            portType: 'inport' or 'outport';
            portName: the name of the port;
            portSignalList: the list of channel signals [irdy,data,trdy];
            signalDirectionList: specify the direction of the signal;
            signalTypeList: signal type, 'wire' or 'vector'
        """
        self.portID = portID
        self.portType = portType
        self.portName = portName
        self.irdy, self.data, self.trdy = [portInputReady, portData, portOutputReady]
        self.irdyType, self.dataType, self.trdyType = 'wire', 'vector', 'wire'
        self.signalTypeList = [self.irdyType, self.dataType, self.trdyType]
        self.irdyDirection, self.dataDirection, self.trdyDirection = 'output', 'output', 'input'
        self.signalDirectionList = [self.irdyDirection, self.dataDirection, self.trdyDirection]

        # Indicate the status of the connectivity
        # 0 for non-connected, 1 for connected
        # This status is useful when generating the testbench with stimuli
        # for the unconnected ports.
        self.portStatus = 0

        # the values for the float signals
        self.signalFloatValueList = [0,0,1]

        # the wire names for each signal when instantiation
        self.irdyInstance, self.dataInstance, self.trdyInstance = self.irdy, self.data, self.trdy
        self.signalList = [self.irdy, self.data, self.trdy]
        self.signalInstanceList = [self.irdyInstance, self.dataInstance, self.trdyInstance]
        # the mask vector indicates whether the signal is to be declared or not, 
        # '1' for enabled, '0' for disabled
        self.maskVector = [1,1,1]

        # the signal attributes, 'irdy, data, trdy'
        # note: the order should be strictly consistent with 'signalList' and
        # 'signalInstanceList'
        self.signalAttributeList = ['irdy', 'data', 'trdy']

    def constructSignalInstanceList(self, maskVector = [1,1,1]):
        self.signalInstanceList = [self.irdyInstance, self.dataInstance, self.trdyInstance]
        self.maskVector = maskVector

    def constructSignalList(self):
        self.signalList = [self.irdy, self.data, self.trdy]

    def constructSignalTypeList(self):
        self.signalTypeList = [self.irdyType, self.dataType, self.trdyType]

    def constructSignalDirectionList(self):
        self.signalDirectionList = [self.irdyDirection, self.dataDirection, self.trdyDirection]

# Define the class to represent a instance of a module
# Two use case: 1, construct the library; 2, construct the blocks in a design
# implementation.
class VerilogInstance:
    # The class representing the HDL instance/Simulink block

    def __init__(self, instanceName = '',
                 moduleType = '',
                 parameterList = [],
                 portList = [],
                 hierarchyNameList = []):
        """
        The construction function of class VerilogInstance.

        Variables:
            instanceName: the name of the instance, unique;
            moduleType: the Verilog module type, e.g. queue_primitive
            portList:  the list of ports of type 'VerilogPort';
            parameterList:  the list of parameters to be configured, of type 'VerilogParameter';
        """
        self.instanceName = instanceName
        self.moduleType = moduleType
        self.parameterList = copy.deepcopy(parameterList)
        self.portList = copy.deepcopy(portList)
        
        self.hierarchyNameList = []  # the hierarchical name

        # clock signals and reset signals
        self.wireClock = 'i_clk'
        self.wireReset = 'i_reset_n'
        self.globalParameterFile = 'global_parameter.v'

        # the list of the parameters to be assigned
        self.parameterAssignmentList = []

        # the name list of the signal to be declared
        self.wireDeclareList = []
        # type of the declared signals, 'wire' or 'vector'
        self.wireTypeList = []

    def extractParameterList(self, keyParameterList, keyValueList):
        """
        This function generate the parameter names and extract the corresponding values to be assigned.

        Set the lists 'parameterAssignmentList' for parameter declaration.
        This list is to be used in generating the module parameter Verilog code and the testbench.
        Variables:
            keyParameterList: the name list of the parameter to be extracted
            keyValueList:     the corresponding value list of the key parameters
        """
        # if the parameter list is empty, then do nothing and return
        if len(self.parameterList) > 0:
            for paraItem in self.parameterList:
                if paraItem.parameterName in keyParameterList:
                    # assign the parameter value of the matched parameter
                    paraItem.parameterValue = keyValueList[keyParameterList.index(paraItem.parameterName)]
                    paraItem.parameterInstanceName = self.instanceName + '_' + paraItem.parameterName
                    # append the parameter to 'parameterAssignmentList'
                    self.parameterAssignmentList.append(copy.deepcopy(paraItem))
        else:
            pass

    def extractWireList(self):
        """
        This function generate the wire names to instantiation the instance.

        Set the lists 'wireDeclareList' and 'wireTypeList' for wire declaration.
        These two lists are to be used in generating the top module Verilog code.
        The wires are those of type 'output' from each port.
        """
        for portItem in self.portList:
            for index in range(len(portItem.signalDirectionList)):
                if (portItem.signalDirectionList[index] == 'output') and (portItem.maskVector[index] == 1):
                    self.wireDeclareList.append(portItem.signalInstanceList[index])
                    self.wireTypeList.append(portItem.signalTypeList[index])

    def setOutputSignals(self):
        """
        This function set the names of all the output signals of this instance
        """
        for portItem in self.portList:  # for each port
            for index, sigDirection in enumerate(portItem.signalDirectionList):
                if sigDirection == 'output':
                    portItem.signalInstanceList[index] = portItem.signalList[index] + '_' + self.instanceName
                else:
                    portItem.signalInstanceList[index] = portItem.signalList[index]
            #for sig, sigInstance, sigDirection in zip(portItem.signalList,
            #portItem.signalInstanceList, portItem.signalDirectionList):
            #    if sigDirection == 'output':
            #        sigInstance = sig + '_' + self.instanceName
            portItem.irdyInstance, portItem.dataInstance, portItem.trdyInstance = tuple(portItem.signalInstanceList)

    # generate the instantiation code in Verilog-HDL
    def printInstance(self, targetFileHandle, indentDepth = 0):
        """
        Print the Verilog instantiation code into the target file.

        targetFileHandle: the file handle to operate the file writing;
        indentDepth: the initial indent depth
        """
        indentValue = indentDepth * TAB_SPACE_RATIO
        # print the instantiation statement of Verilog
        targetFileHandle.write(' ' * indentValue + '// Instance ' + self.instanceName + '\n')
        targetFileHandle.write(' ' * indentValue + self.moduleType + ' ')

        # print the parameter instantiation, only when the parameter assignment
        # list is not null
        if self.parameterAssignmentList:
            tmpStr = '#('
            for paraItem in self.parameterAssignmentList:
                tmpStr += '.' + paraItem.parameterName + '(' + paraItem.parameterInstanceName + '),'
            targetFileHandle.write(tmpStr[:-1] + ')\n')   # replace the last ',' with ')'
            indentValue += TAB_SPACE_RATIO  # add a tab indent
            targetFileHandle.write(' ' * indentValue)
        targetFileHandle.write(self.instanceName + '(\n')
        indentValue += TAB_SPACE_RATIO  # add a tab indent

        # print the signal binding (wire instantiations)
        for portItem in self.portList:  # each port
            for signalIndex in range(len(portItem.signalList)):  # each signal
                tmpStr = '.' + portItem.signalList[signalIndex] + '(' + portItem.signalInstanceList[signalIndex] + '),'
                targetFileHandle.write(' ' * indentValue + tmpStr + '\n')
        targetFileHandle.write(' ' * indentValue + '.i_clk(' + self.wireClock + '),\n')
        targetFileHandle.write(' ' * indentValue + '.i_reset_n(' + self.wireReset + '));\n\n')

    # Bypass the inport or outport module by connecting the input 
    # channel and the output channel with wires
    def bypassPrintPortInst(self, targetFileHandle, indentDepth = 0):
        """
        Print the bypass code for the port instances by using "assign" statements in Verilog

        targetFileHandle: the file handle to operate the file writing;
        indentDepth: the initial indent depth
        """
        indentValue = indentDepth * TAB_SPACE_RATIO
        targetFileHandle.write(' ' * indentValue + '// Bypass the port ' + self.instanceName + '\n')
        if len(self.portList) != 2: # the number of ports is not 2
            assert('Wrong number of ports in module ' + self.instanceName + '!')
        else:
            port1 = self.portList[0]
            port2 = self.portList[1]
            for sigIndex in range(len(port1.signalList)):
                if (port1.signalDirectionList[sigIndex] == 'input') and (port2.signalDirectionList[sigIndex] == 'output'):
                    tmpStr = 'assign ' + port2.signalInstanceList[sigIndex] + ' = ' + port1.signalInstanceList[sigIndex] + ';'
                elif (port1.signalDirectionList[sigIndex] == 'output') and (port2.signalDirectionList[sigIndex] == 'input'):
                    tmpStr = 'assign ' + port1.signalInstanceList[sigIndex] + ' = ' + port2.signalInstanceList[sigIndex] + ';'
                else:
                    assert('Wrong direction of signal in module ' + self.instanceName + '!')
                targetFileHandle.write(' ' * indentValue + tmpStr + '\n')
            targetFileHandle.write(' ' * indentValue + '\n')


    def printParameter(self, targetFileHandle, indentDepth = 0):
        """
        Print the parameter assignments for this instance.

        This function print the parameter assignments into the target file, and return the indent depth
        Input Parameters:
        targetFileHandle: the file handle to operate the file writing;
        indentDepth: the initial indent depth
        """
        if self.parameterAssignmentList:
            indentValue = indentDepth * TAB_SPACE_RATIO
            targetFileHandle.write(' ' * indentValue + '// ' + '$'.join(self.hierarchyNameList+[self.instanceName]) + '\n')
            # print the instantiation statement of Verilog
            paraType = ''
            for paraItem in self.parameterAssignmentList:
                if paraItem.parameterType == 'real':     # real parameter
                    paraType = 'real'
                tmpStr = 'parameter ' + paraType + ' ' + paraItem.parameterInstanceName + ' = ' + str(paraItem.parameterValue) + ';'
                targetFileHandle.write(' ' * indentValue + tmpStr + '\n')
            targetFileHandle.write('\n')

    def printWireDeclaration(self, targetFileHandle, indentDepth = 0):
        """
        Print the wire declaration for this instance.

        This function print the parameter assignments into the target file, and return the indent depth
        Input Parameters:
        targetFileHandle: the file handle to operate the file writing;
        indentDepth: the initial indent depth
        """
        if self.wireDeclareList:
            indentValue = indentDepth * TAB_SPACE_RATIO
            targetFileHandle.write(' ' * indentValue + '// ' + self.instanceName + '\n')
            # print the wire declaration statement of Verilog
            tmpStr = ''
            for wireIndex in range(len(self.wireDeclareList)):
                if self.wireTypeList[wireIndex] == 'wire':
                    tmpStr = 'wire ' + self.wireDeclareList[wireIndex] + ';'
                elif self.wireTypeList[wireIndex] == 'vector':
                    tmpStr = 'wire [PACKET_LENGTH-1:0] ' + self.wireDeclareList[wireIndex] + ';'
                else:
                    assert('Type error of the wire signal in module ' + self.instanceName)
                targetFileHandle.write(' ' * indentValue + tmpStr + '\n')
            targetFileHandle.write('\n')

# Define the class to generate the Verilog HDL file according to the
# configuration files.
class SimulinkToVerilog:
    # The class of generating the HDL files

    def __init__(self,
                 libFileList = [],
                 cfgFileList = [],
                 mdlSystemList = []):
        """
        The construction function of class SimulinkToVerilog.

        Class Variables:
            systemName:   the name of the top module (file name);
            hierarchyNameList:  the name list of the moduls of upper layers 
                                when generating HDL code hierarchically
            libFileList:  the list of the library content;
            cfgFileList:  the list of the configuration content (mapping rules);
            mdlSystemList:  the parsed list of the System of the Simulink mdl file;
            instanceList: the list of modules to be instantiated;
            connectList:  the list of channels to interconnect the modules;
            parameterList: the parameter list of the system to be configured;
        """
        self.systemName = ''         # top module file
        self.testbenchName = ''     # testbench file
        self.hierarchyNameList = []  # the hierarchical name

        self.libFileList = libFileList
        self.cfgFileList = cfgFileList
        self.mdlSystemList = mdlSystemList

        self.libFilePath = ''
        self.cfgFilePath = ''
        self.mdlFilePath = ''

        self.targetDirectory = ''   # the directory to place the generated Verilog files
        self.targetFileName = self.systemName # the moudle name/file name
        self.targetFile = ''

        self.generateLib = False    # the flag to indicate whether generate the vlib description or not

        # The dictionary of the modules in the libarary file
        # with the format {'(module_name,inport_num,outport_num)', instance}
        # By matching the tuple composed of module name and the port numbers,
        # the instance of type 'VerilogInstance' is found
        self.libraryModuleDict = {}

        # the mapping rules
        self.mappingRuleDict = {}

        # the parsed system list of the simulink model
        self.systemList = []

        self.instanceList = []
        self.connectList = []

        # the parameters of the system to be configured
        self.parameterList = []
        # the portList of the system
        self.portList = []

    # Parse the library file and transfer the library into a list
    def constructLibraryList(self):
        """
        This function obtain the list after parsing the library file
        """
        resultLib = configurationParser(self.libFilePath)
        self.libFileList = resultLib.asList()

    # Construct the dictionary structure of the library according to the
    # content in 'libFileList'
    def constructLibrary(self):
        """
        Parse the library file and construct the corresponding data structure
        """
        #from pprint import pprint
        #pprint(self.libFileList)
        for libItem in self.libFileList:
            if libItem[0] == 'Global':  # the keyword 'Global'
                for globalItem in libItem[1:]:
                    self.libraryModuleDict[globalItem[0]] = globalItem[1]
            elif libItem[0] == 'Module':    # the keyword 'Module'
                tmpInstance = VerilogInstance()
                tmpPortList = []
                tmpParameterList = []
                inportNum = 0
                outportNum = 0
                inportCount = 0
                outportCount = 0
                for moduleItem in libItem[1:]:
                    if moduleItem[0] == 'Name':     # module name
                        tmpInstance.moduleType = moduleItem[1]
                        tmpInstance.instanceName = moduleItem[1]
                    elif moduleItem[0] == 'Ports':
                        #inportNum, outportNum = eval(moduleItem[1])
                        inportNum, outportNum = moduleItem[1]
                    elif moduleItem[0] == 'Inport': # inport description
                        inportCount += 1
                        tmpInport = VerilogPort()
                        tmpInport.portType = 'inport'
                        for portItem in moduleItem[1:]:
                            if portItem[0] == 'PortID':
                                tmpInport.portID = portItem[1]
                            else:   # 'Input' or 'Output' keyword
                                # judge the signal name and make the assignments
                                if portItem[1][1] == 'irdy':
                                    tmpInport.irdy = portItem[1][2]
                                    tmpInport.irdyDirection = portItem[0].lower()
                                    tmpInport.irdyType = portItem[1][0]
                                elif portItem[1][1] == 'data':
                                    tmpInport.data = portItem[1][2]
                                    tmpInport.dataDirection = portItem[0].lower()
                                    tmpInport.dataType = portItem[1][0]
                                elif portItem[1][1] == 'trdy':
                                    tmpInport.trdy = portItem[1][2]
                                    tmpInport.trdyDirection = portItem[0].lower()
                                    tmpInport.trdyType = portItem[1][0]
                                else:
                                    assert('The signal type ' + portItem[1][1] + 'in ' + moduleItem[0] + ' is not valid!')
                        tmpInport.constructSignalDirectionList()
                        tmpInport.constructSignalTypeList()
                        tmpInport.constructSignalList()
                        tmpPortList.append(copy.deepcopy(tmpInport))

                    elif moduleItem[0] == 'Outport': # outport description
                        outportCount += 1
                        tmpOutport = VerilogPort()
                        tmpOutport.portType = 'outport'
                        for portItem in moduleItem[1:]:
                            if portItem[0] == 'PortID':
                                tmpOutport.portID = portItem[1]
                            else:   # 'Input' or 'Output' keyword
                                # judge the signal name and make the
                                                                 # assignments
                                if portItem[1][1] == 'irdy':
                                    tmpOutport.irdy = portItem[1][2]
                                    tmpOutport.irdyDirection = portItem[0].lower()
                                    tmpOutport.irdyType = portItem[1][0]
                                elif portItem[1][1] == 'data':
                                    tmpOutport.data = portItem[1][2]
                                    tmpOutport.dataDirection = portItem[0].lower()
                                    tmpOutport.dataType = portItem[1][0]
                                elif portItem[1][1] == 'trdy':
                                    tmpOutport.trdy = portItem[1][2]
                                    tmpOutport.trdyDirection = portItem[0].lower()
                                    tmpOutport.trdyType = portItem[1][0]
                                else:
                                    assert('The signal type ' + portItem[1][1] + 'in ' + moduleItem[0] + ' is not valid!')
                        tmpOutport.constructSignalDirectionList()
                        tmpOutport.constructSignalTypeList()
                        tmpOutport.constructSignalList()
                        tmpPortList.append(copy.deepcopy(tmpOutport))

                    elif moduleItem[0] == 'Parameter':
                        tmpPara = VerilogParameter()
                        for paraItem in moduleItem[1:]:
                            tmpPara.parameterName = paraItem[0]
                            tmpPara.parameterInstanceName = paraItem[0]
                            tmpPara.parameterType = paraItem[1][0]
                            tmpPara.defaultValue = paraItem[1][1]
                            tmpParameterList.append(copy.deepcopy(tmpPara))
                    else:
                        assert('The keyword ' + moduleItem[0] + ' is invalid!')

                # judge the validity of the port amount
                if inportNum != inportCount:
                    assert('The inport number configuration doesnot match in module ' + moduleItem[0])
                if outportNum != outportCount:
                    assert('The outport number configuration doesnot match in module ' + moduleItem[0])
                # construct the module instance for the library
                tmpInstance.portList = copy.deepcopy(tmpPortList)
                tmpInstance.parameterList = copy.deepcopy(tmpParameterList)

                # add this module into the library dictionary
                self.libraryModuleDict.setdefault((tmpInstance.moduleType, inportNum, outportNum),tmpInstance)
            else:
                assert('The keyword ' + libItem[0] + ' is not valid in the library file')

    # Construct the mapping file
    def constructMappingList(self):
        """
        This function obtain the list after parsing the mapping file
        """
        resultMap = configurationParser(self.cfgFilePath)
        self.cfgFileList = resultMap.asList()

    # Construct the dictionary structure of the mapping rules
    def constructMapping(self):
        """
        This function obtain the list after parsing the mapping file
        """
        for mapRule in self.cfgFileList:    # each 'Rule' block
            tmpParameterDict = {}
            simulinkBlock = ''
            verilogModule = ''
            inportNum, outportNum = 0, 0
            if mapRule[0] != 'Rule':
                assert('The keyword ' + mapRule[0] + ' is not valid!')
            else:
                for ruleItem in mapRule[1:]:    # each rule item
                    if ruleItem[0] == 'Simulink_Block':
                        simulinkBlock = ruleItem[1]
                    elif ruleItem[0] == 'Verilog_Module':
                        verilogModule = ruleItem[1]
                    elif ruleItem[0] == 'Ports':
                        inportNum, outportNum = ruleItem[1]
                    elif ruleItem[0] == 'Parameter':
                        for paraRule in ruleItem[1:]:
                            tmpParameterDict[paraRule[0]] = paraRule[1]
                    else:
                        assert('The keyword ' + ruleItem[0] + ' is not valid in the mapping file!')
                # add this map rule into the dictionary
                # {(simulinkBlock, inporNum, outportNum): [(verilogModule,
                # inportNum, outportNum), parameterDict]}
                self.mappingRuleDict.setdefault((simulinkBlock, inportNum, outportNum), [(verilogModule, inportNum, outportNum), copy.deepcopy(tmpParameterDict)])

    ####### Several tool functions to help generate the Verilog module ####
    # Construct an instance of a port block of "Inport" or "Outport"
    def generatePortInst(self, portBlockList):
        """
        Construct an instance of a port block of type "Inport" or "Outport"

        The list of a port ("Inport" or "Outport") block  is parsed to generate a port instance.
        Variable:
            portBlockList: list of the block information from the parsed model file
        """
        tmpInstance = VerilogInstance()

        # 1 inport and 1 outport by default
        tmpInport = VerilogPort()
        tmpInport.portID = 1
        tmpInport.portType = 'inport'

        tmpOutport = VerilogPort()
        tmpOutport.portID = 1
        tmpOutport.portType = 'outport'

        tmpPortID = 1
        for blockItem in portBlockList[1:]:
            if blockItem[0] == 'BlockType':  # 'Inport' or 'Outport'
                tmpInstance.moduleType = blockItem[1]
            elif blockItem[0] == 'Name':
                tmpInstance.instanceName = blockItem[1]
                tmpInport.portName = blockItem[1]
            elif blockItem[0] == 'Port':
                if isinstance(blockItem[1], (str, int) ):
                    tmpPortID = int(blockItem[1])

        if tmpInstance.moduleType == 'Inport':
            tmpInport.portID = tmpPortID
        elif tmpInstance.moduleType == 'Outport':
            tmpOutport.portID = tmpPortID
        else:
            pass

        # set the signal names of the ports
        tmpInport.irdy = 'i_irdy_inport'
        tmpInport.data = 'iv_data_inport'
        tmpInport.trdy = 'o_trdy_inport'
        tmpInport.irdyDirection, tmpInport.dataDirection, tmpInport.trdyDirection = 'input', 'input', 'output'
        tmpInport.irdyInstance, tmpInport.dataInstance, tmpInport.trdyInstance = tmpInport.irdy, tmpInport.data, tmpInport.trdy

        tmpOutport.irdy = 'o_irdy_outport'
        tmpOutport.data = 'ov_data_outport'
        tmpOutport.trdy = 'i_trdy_outport'
        tmpOutport.irdyDirection, tmpOutport.dataDirection, tmpOutport.trdyDirection = 'output', 'output', 'input'
        tmpOutport.irdyInstance, tmpOutport.dataInstance, tmpOutport.trdyInstance = tmpOutport.irdy, tmpOutport.data, tmpOutport.trdy

        tmpInport.constructSignalDirectionList()
        tmpInport.constructSignalTypeList()
        tmpInport.constructSignalList()
        tmpInport.constructSignalInstanceList()

        tmpOutport.constructSignalDirectionList()
        tmpOutport.constructSignalTypeList()
        tmpOutport.constructSignalList()
        tmpOutport.constructSignalInstanceList()

        # add the two ports to the instance
        tmpInstance.portList.append(copy.deepcopy(tmpInport))
        tmpInstance.portList.append(copy.deepcopy(tmpOutport))

        return copy.deepcopy(tmpInstance)

    # Construct the parameter list
    def constructSystemParameterList(self):
        """
        Extract the parameters of this system.

        The parameters of the system include all the parameters that are
        assigned in each instance. The prefix of the hierarchy is added to 
        the parameter name.
        """
        for instItem in self.instanceList:
            for paraItem in instItem.parameterAssignmentList:
                paraItem.parameterInstanceName = '$'.join([self.systemName, paraItem.parameterInstanceName])
            self.parameterList.extend(copy.deepcopy(instItem.parameterAssignmentList))
        for paraItem in self.parameterList:
            paraItem.parameterName = paraItem.parameterInstanceName

    # extract the list file of the system in the mdl file
    def constructMdlList(self):
        resultMDL = mdlParser(self.mdlFilePath)
        mdlData = resultMDL.asList()
        self.systemList = getSectionList(mdlData, 'System')

    # construct the list of all instances in the system
    # subsystem is instantiated recursively
    def constructInstanceList(self, sysList = []):
        """
        Construct all the instances of type 'VerilogInstance'.

        Each time a block is obtained, a instance of the corresponding
        Verilog instance is appended to the list through the mapping rules
        Variable:
            flag: '0' for top system, otherwise for subsystem
        """
        # First, create all the instances, in case of subsystem, generate it
        #   the parameters are instantiated.
        # Second, instantiate the port signals according to the lines in
        #   the Simulink model.
        # Last, extract the blocks in the system
        if not sysList:
            sysMdlList = self.systemList
        else:
            sysMdlList = sysList

        for mdlBlock in sysMdlList[1:]:    # each block
            if  mdlBlock[0] == 'Name':
                #if not self.systemName:
                #    self.systemName = mdlBlock[1]
                #else:
                #    self.systemName = self.systemName + '$' + mdlBlock[1]
                self.systemName = mdlBlock[1]
            elif mdlBlock[0] == 'Block':    # a block description
                # the block sections
                blockType = ''      # 'Reference', 'Subsystem', 'Inport', 'Outport'
                sourceType = ''
                instanceName = ''
                inportNum, outportNum = 0, 0
                sourceType = ''
                # first get the block type and the port information
                for blockItem in mdlBlock[1:]:  # each parameter of a block
                    if blockItem[0] == 'BlockType':
                        blockType = blockItem[1]
                    elif blockItem[0] == 'Name':
                        instanceName = blockItem[1]
                    elif blockItem[0] == 'Ports':
                        # In case that the block has no outputs, then append a
                        # '0'
                        if len(blockItem[1]) < 2:
                            blockItem[1] += [0]
                        inportNum, outportNum = blockItem[1]
                    elif blockItem[0] == 'SourceType':
                        sourceType = blockItem[1]
                    else:
                        pass    # ignore the other keywords

                if blockType == 'Reference':
                    # find the parameter names from the mapping rules
                    tmpMapList = self.mappingRuleDict[(sourceType, inportNum, outportNum)]
                    # get the parameter mapping list
                    tmpParaDict = tmpMapList[1]
                    # 'Instantiate' the verilog module instance from the
                    # library
                    tmpInstance = copy.deepcopy(self.libraryModuleDict[tmpMapList[0]])
                    tmpInstance.instanceName = instanceName

                    paraNameList = []
                    paraValueList = []
                    # find the paramters
                    for blockItem in mdlBlock[1:]:
                        if blockItem[0] in tmpParaDict:
                            # judge the type of 'list', for example
                            # the parameter names are stored as ['WEIGHT_A',
                            # 'WEIGHT_B'] for 'Merge'
                            if isinstance(tmpParaDict[blockItem[0]], list):
                                paraNameList += tmpParaDict[blockItem[0]]
                            else:
                                paraNameList += [tmpParaDict[blockItem[0]]]

                            #Note: specially deal with the switch primitive
                            if (blockItem[0] == 'flowIDPort'):
                                # extract from '{[1], [2]}'
                                tmpValue = eval(blockItem[1].replace('{', '').replace('}', ''))
                                paraValueList += [tmpValue[0][0]]
                            else:
                                # first covert the value into a string, then
                                # evaluate the string
                                # this can deal with the case of '[1,1]'
                                tmpValue = str(blockItem[1]).replace('[', '').replace(']', '').replace(',', ' ').split()
                                for strValue in tmpValue:
                                    try:
                                        paraValueList += [eval(strValue)]
                                    except:
                                        paraValueList += [strValue]
                    # set the parameters to be assigned in this instance
                    tmpInstance.extractParameterList(paraNameList, paraValueList)
                    # add this instance into the instance list
                    self.instanceList.append(copy.deepcopy(tmpInstance))
                elif blockType in ['Inport', 'Outport']:    # port block
                    # generate the instance of a port
                    portInstance = self.generatePortInst(mdlBlock)
                    self.instanceList.append(copy.deepcopy(portInstance))
                elif blockType == 'SubSystem':
                    ####### note: the parameters in the Simulink mask of
                    ####### subsystem are not supported currently #######
                    tmpSys = SimulinkToVerilog()
                    tmpSys.libFilePath = self.libFilePath
                    tmpSys.cfgFilePath = self.cfgFilePath
                    tmpSys.targetDirectory = self.targetDirectory
                    tmpSys.generateLib = self.generateLib
                    # transfer the current system name to the next level
                    tmpSys.systemName = self.systemName
                    tmpSys.hierarchyNameList = self.hierarchyNameList + [self.systemName]
                    # obtain the mdl data list from current block
                    subSysSection = getSectionList(mdlBlock,'System')
                    # generate the system recursively
                    tmpSysInst = tmpSys.generateSystem(subSysSection)
                    self.instanceList.append(copy.deepcopy(tmpSysInst))
                else:
                    pass
            else:
                pass    # for the other cases, just ingnore

        # set the names of all the output signals for all the instances
        for instanceItem in self.instanceList:
            instanceItem.setOutputSignals()

        # note: before dealing with lines, all the instances have been listed
        # instatiate the port signals according to the lines,
        # only deal with all the output signals
        for mdlLine in self.systemList[1:]:    # each line
            if mdlLine[0] == 'Line':    # a line description
                lineName = ''
                sourceBlock = ''
                sourcePort = 0
                destBlock = []
                destPort = []
                # flag to indicate the branch existence
                flagBranch = 0
                for lineItem in mdlLine[1:]:
                    if lineItem[0] == 'Name':
                        lineName = lineItem[1]
                    elif lineItem[0] == 'SrcBlock':
                        sourceBlock = lineItem[1]
                    elif lineItem[0] == 'SrcPort':
                        sourcePort = lineItem[1]
                    elif lineItem[0] == 'DstBlock':
                        destBlock += [lineItem[1]]
                    elif lineItem[0] == 'DstPort':
                        destPort += [lineItem[1]]
                    elif lineItem[0] == 'Branch':   # parse the branches
                        flagBranch += 1
                        for branchItem in lineItem[1:]:
                            if branchItem[0] == 'DstBlock':
                                destBlock += [branchItem[1]]
                            elif branchItem[0] == 'DstPort':
                                destPort += [branchItem[1]]
                            else:
                                pass
                    else:
                        pass

                ###########################################################
                # set the names of the input signals of the instances
                # if the line has a name, update the corresponding output
                # signal name

                tmpSourcePort = ''
                # format the name string
                tmpName = "_".join(lineName.split()).replace('\\n', '').replace('\\t', '').replace('\\', '')
                for instItem in self.instanceList:
                    if instItem.instanceName == sourceBlock:    # find the source block
                        findSourceFlag = 0
                        for portItem in instItem.portList:  # go though all the ports
                            # find the output port of the source block
                            if (portItem.portID == sourcePort) and (portItem.portType == 'outport') :
                                portItem.portStatus = 1    # update the port connection status
                                tmpSourcePort = portItem
                                findSourceFlag = 1
                                if not lineName:    # the line is not named
                                    pass
                                else:   # the line is named, update the output signal name
                                    portItem.irdyInstance = tmpName + '_irdy'
                                    portItem.dataInstance = tmpName + '_data'
                                    portItem.trdyInstance = tmpName + '_trdy'
                                    portItem.constructSignalInstanceList()
                                break   # stop searching the port
                        if findSourceFlag == 0:   # the 'tmpSourcePort' is null, means the source port is not found, error!
                            assert('The port ' + str(sourcePort) + ' of source ' + sourceBlock + ' is not found! The dest block is ' + str(destBlock) + ' with port ' + str(destPort))
                        break   # stop searching the other instances, "they must be only one source block"

                ###########################################################
                # next, deal with the destination instances
                destCount = 1
                for instItem in self.instanceList:
                    if instItem.instanceName in destBlock:
                        # set the signal names for this block
                        tmpDestPort = destPort[destBlock.index(instItem.instanceName)]
                        for portItem in instItem.portList:
                            if (portItem.portID == tmpDestPort) and (portItem.portType == 'inport'):
                                portItem.portStatus = 1    # update the port connection status
                                if not lineName:    # the line is not named
                                    if flagBranch > 0:  # if there is a branch, all the 'trdy' signals have the same name
                                        tmpSourcePort.trdyInstance = '_'.join(destBlock) + '_trdy'
                                        portItem.trdyInstance = '_'.join(destBlock) + '_trdy'
                                    else:   # there is no branch
                                        tmpSourcePort.trdyInstance = portItem.trdyInstance
                                else:   # in case the line is named
                                    portItem.trdyInstance = tmpSourcePort.trdyInstance

                                portItem.irdyInstance = tmpSourcePort.irdyInstance
                                portItem.dataInstance = tmpSourcePort.dataInstance
                                tmpSourcePort.constructSignalInstanceList()
                                ####################################################################
                                ###### the trdy signal should be further dealt
                                ###### with, because multi-trdy (multi-driven)
                                ###### signals are actually wire-and in Verilog
                                if destCount == 1:
                                    # only the first dest instance will declare
                                    # the 'trdy' signal
                                    portItem.constructSignalInstanceList([1,1,1])
                                    destCount += 1  # disable it
                                else:
                                    portItem.constructSignalInstanceList([1,1,0])

                                break   # stop searching the port
            else:
                # do nothing for the other keywords not descibing a 'Line'
                pass
        # construct the signal instance for all the floating ports
        for instItem in self.instanceList:
            for portItem in instItem.portList:
                if portItem.portStatus == 0:    # the port is floating
                    if portItem.portType == 'inport':    # a input port
                        portItem.irdyInstance = portItem.irdyInstance + '_' + instItem.instanceName
                        portItem.dataInstance = portItem.dataInstance + '_' + instItem.instanceName
                    else:   # a output port
                        portItem.trdyInstance = portItem.trdyInstance + '_' + instItem.instanceName
                    portItem.constructSignalInstanceList()

        # construct the wire declaration list for each instance
        for instanceItem in self.instanceList:
            instanceItem.extractWireList()

    def printHeader(self, targetFileHandle, indentDepth = 0, module_name = '', file_type = 'module'):
        """
        Print the header of the Verilog testbench.

        :param targetFileHandle: the file handle to operate the file writing;
        :param indentDepth: the initial indent depth.
        :param module_name: the name of the module
        :param file_type: module, testbench, library description
        """
        import datetime
        today = datetime.date.today()
        indentValue = indentDepth * TAB_SPACE_RATIO
        if not module_name:
            module_name = self.targetFileName
        if file_type == 'library description':
            suffix = 'V.vlib'
        else:
            suffix = 'V'

        tmpStrList = ["// +FHEADER--------------------------------------------------------------",
            "// Copyright (C) {fileYear} Xueqian Zhao.  All rights reserved.          ".format(fileYear = today.strftime('%Y')),
            "// ----------------------------------------------------------------------",
            "// FILE NAME :  {fileName}.{ftype}                                      ".format(fileName = module_name,ftype=suffix),
            "// DEPARTMENT : ESY/ICT/KTH Royal Institute of Technology                 ",
            "// AUTHOR: Xueqian Zhao                                                  ",
            "// AUTHOR'S EMAIL : xueq@kth.se                                          ",
            "// ----------------------------------------------------------------------",
            "// RELEASE HISTORY                                                       ",
            "// VERSION DATE AUTHORDESCRIPTION                                        ",
            "// 1.0 {fileDate} Xueqian Zhao, created                                  ".format(fileDate = today.strftime('%Y-%m-%d')),
            "// ----------------------------------------------------------------------",
            "// KEYWORDS :                                                            ",
            "// ----------------------------------------------------------------------",
            "// PURPOSE :                                                             ",
            "//   The Verilg {fileType} of xMAS model '{fileName}'.                   ".format(fileType = file_type, fileName = self.targetFileName),
            "// -FHEADER--------------------------------------------------------------"]
        for item in tmpStrList:
            targetFileHandle.write(' ' * indentValue + item + '\n')
        targetFileHandle.write('\n')

    def printDirective(self, targetFileHandle, indentDepth = 0):
        """
        Print the directive of the Verilog testbench.

        targetFileHandle: the file handle to operate the file writing;
        indentDepth: the initial indent depth
        """
        indentValue = indentDepth * TAB_SPACE_RATIO
        tmpStr = ' ' * indentValue + '`ifndef CLK_PERIOD_TOP \n' \
               + ' ' * (indentValue + TAB_SPACE_RATIO) + '`define CLK_PERIOD_TOP 2\n'\
               + ' ' * indentValue + '`endif \n\n' \
               + ' ' * indentValue + '`timescale 1ns/1ns'
        targetFileHandle.write(tmpStr + '\n')

    # Construct the port list of the system, the ports are specified explicitly by default
    #
    def constructPortList(self, explicitPort = True):
        """
        Construct the list of ports to be declared in the Verilog module file

        explicitPort: indicates how the ports are specified.
            'True': the ports are specified explicitly by block "Inport" or "Outport"
            'False': take the unconnected ports as the ports of the system besides the specified ones.
        The list cntPorts is returned to indicate the number of input ports and output ports.
        """
        cntPorts = [0, 0]   # '[inport, outport]'
        if explicitPort:
            # the ports of the subsystem in Simulink are marked by 'inport' and
            # 'outport' components
            for instItem in self.instanceList:
                if instItem.moduleType in ['Inport', 'Outport']:
                    for portItem in instItem.portList:
                        if portItem.portType == instItem.moduleType.lower():
                            portItem.portName = self.systemName + '_' + instItem.instanceName
                            portItem.signalList = [portItem.portName + '_' + signalItem for signalItem in portItem.signalList]
                            self.portList.append(copy.deepcopy(portItem))  # add this port
                            if portItem.portType == 'inport':
                                cntPorts[0] = cntPorts[0] + 1
                            elif portItem.portType == 'outport':
                                cntPorts[1] = cntPorts[1] + 1
                            else:
                                pass
        else:
            # generate the top module, take the unconnected channels as the
            # port
            # the registers (stimuli) for floating signals
            # go though each instance, and add the declaration
            for instItem in self.instanceList:
                for portItem in instItem.portList:
                    if portItem.portStatus == 0:    # floating port
                        portItem.portName = self.systemName + '_' + instItem.instanceName
                        self.portList.append(copy.deepcopy(portItem))  # add this port
                        if portItem.portType == 'inport':
                            cntPorts[0] = cntPorts[0] + 1
                        elif portItem.portType == 'outport':
                            cntPorts[1] = cntPorts[1] + 1
                        else:
                            pass

        return cntPorts

    def printLibDescription(self, cntPorts = [0,0], fileLib = '', explicitPort = True):
        """
        Generate the library description of the module, and write into a text file

        fileLib: the file name of the library description file, it is the target file.
        explicitPort: indicates how the ports are specified.
            'True': the ports are specified explicitly by block "Inport" or "Outport"
            'False': take the unconnected ports as the ports of the system besides the specified ones.
        The list cntPorts is returned to indicate the number of input ports and output ports.
        """
        # set the target lib file by adding the file extension to the module
        # file name
        if not fileLib:    # the file name is not specified
            libDescriptionName = self.targetFile + '.vlib'
        else:
            libDescriptionName = fileLib + '.vlib'

        with open(libDescriptionName, 'w+') as f:
            curIndent = lambda x: ' ' * TAB_SPACE_RATIO * x # x is an integer
            quotedStr = lambda x: '"' + str(x) + '"' # x is a string
            strList = lambda x: '[' + ', '.join([str(y) for y in x]) + ']' # x is a list of strings
            indentDepth = 0
            cntInport = 0
            cntOutport = 0
            # print the header of the module
            self.printHeader(f,indentDepth,file_type='library description')
            f.write(curIndent(indentDepth) + 'Module {\n')
            indentDepth = indentDepth + 1
            f.write(curIndent(indentDepth) + 'Name\t' + quotedStr(self.targetFileName) + '\n')
            f.write(curIndent(indentDepth) + 'Ports\t' + strList(cntPorts) + '\n')

            # print the port information
            # the port ID is the sequence number of the inports/outports
            if self.portList:
                portID = 0
                for portItem in self.portList:
                    if portItem.portType == 'inport':
                        cntInport = cntInport + 1
                        f.write(curIndent(indentDepth) + 'Inport {\n')
                        if explicitPort:    # if the port is explicitly specified, the portID is determined
                            portID = portItem.portID
                        else:
                            portID = cntInport
                    elif portItem.portType == 'outport':
                        cntOutport = cntOutport + 1
                        f.write(curIndent(indentDepth) + 'Outport {\n')
                        if explicitPort:    # if the port is explicitly specified, the portID is determined
                            portID = portItem.portID
                        else:
                            portID = cntOutport
                    else:
                        pass
                    indentDepth = indentDepth + 1
                    f.write(curIndent(indentDepth) + 'PortID\t' + str(portID) + '\n')

                    portSignalList = [portItem.portName + '_' + signalItem for signalItem in portItem.signalList]
                    for direction, type, signal, name in zip(portItem.signalDirectionList, portItem.signalTypeList, portItem.signalAttributeList, portSignalList):
                        tmpStr = direction.capitalize() + '\t' + strList([quotedStr(str(y)) for y in [type, signal, name] ]) + '\n'
                        f.write(curIndent(indentDepth) + tmpStr)
                    indentDepth = indentDepth - 1
                    f.write(curIndent(indentDepth) + '}\n')
            # print the parameter information
            if self.parameterList:
                f.write(curIndent(indentDepth) + 'Parameter {\n')
                indentDepth = indentDepth + 1
                for paraItem in self.parameterList:
                    tmpStr = paraItem.parameterInstanceName + '\t' + strList([quotedStr(paraItem.parameterType),quotedStr(paraItem.parameterValue) if paraItem.parameterType == 'string' else paraItem.parameterValue]) + '\n'
                    f.write(curIndent(indentDepth) + tmpStr)
                indentDepth = indentDepth - 1
                f.write(curIndent(indentDepth) + '}\n')

            indentDepth = indentDepth - 1
            f.write(curIndent(indentDepth) + '}\n')

    def generateSystem(self, mdlList = [], hierarchicalName = True):
        """
        Generate the top Verilog module of the system/subsystem

        Note: The 'instanceList' shoule be well constructed before generating
          the Verilog module, otherwise the information may be wrong.
        Variables:
            mdlList: the mdl data list of current system;
            generateLib: boolean, 'True' by default, indicating whether to generate the lib file of current system;
            hierarchicalName: boolean, 'True' by default, indicating whether to use the hierarchical module name when generating HDL code.
        """
        # set up the library, mapping rules and the system model data list
        if not self.libraryModuleDict:
            self.constructLibraryList()
            self.constructLibrary()

        if not self.mappingRuleDict:
            self.constructMappingList()
            self.constructMapping()

        if mdlList:
            self.systemList = mdlList
        elif not self.systemList:
            self.constructMdlList()

        # Construct the instance list, recursively
        self.constructInstanceList()
        
        # set the moudule name and the file name
        if hierarchicalName:
            if not self.hierarchyNameList:
                self.targetFileName = self.systemName
            else:
                self.targetFileName = '$'.join(self.hierarchyNameList + [self.systemName])
        else:
            self.targetFileName = self.systemName

        # first judge the 'targetDirectory'
        if not self.targetDirectory:
            self.targetDirectory = os.getcwd()
        else:
            if not os.path.exists(self.targetDirectory):    # if the target directory is not exist
                os.makedirs(self.targetDirectory)   # create the directory
        self.targetFile = os.path.join(self.targetDirectory, self.targetFileName + '.V')

        # construct the port list with default type: specify the ports explicitly
        cntPortList = self.constructPortList()
        # construct the parameter list of this system
        self.constructSystemParameterList()

        with open(self.targetFile, 'w+') as f:
            indentDepth = 0
            # print the header of the module
            self.printHeader(f,indentDepth,file_type='module')
            indentDepth += 1
            curIndent = lambda x: ' ' * TAB_SPACE_RATIO * x

            # print the module declaration
            tmpStr = 'module ' + self.targetFileName + '('
            f.write(tmpStr + '\n')
            # print the interface of the module
            if self.portList:   # only when the portlist exists
                for portItem in self.portList:
                    tmpStr = ", ".join(portItem.signalList)
                    f.write(curIndent(indentDepth) + tmpStr + ',\n')
            f.write(curIndent(indentDepth) + self.libraryModuleDict['Clock'] + ', ' + self.libraryModuleDict['Reset'] + ');\n\n')
            f.write(curIndent(indentDepth) + '`include "' + self.libraryModuleDict['GlobalParameterFile'] + '"\n\n')

            # print the parameter declarations
            for instItem in self.instanceList:
                instItem.printParameter(f, indentDepth)

            if self.portList:
                # print the port signal declaration
                for portItem in self.portList:
                    for type, name, direction in zip(portItem.signalTypeList, portItem.signalList, portItem.signalDirectionList):
                        if type == 'wire':
                            f.write(curIndent(indentDepth) + direction + ' ' + name + ';\n')
                        elif type == 'vector':
                            f.write(curIndent(indentDepth) + direction + ' [PACKET_LENGTH-1:0]  ' + name + ';\n')
                        else:
                            assert('Signal type not identified: ' + type)
                    f.write('\n')
                f.write(curIndent(indentDepth) + 'input ' + self.libraryModuleDict['Clock'] + ';\n')
                f.write(curIndent(indentDepth) + 'input ' + self.libraryModuleDict['Reset'] + ';\n')
                f.write('\n')

            # print the wire declaration of the interconnects
            for instItem in self.instanceList:
                instItem.printWireDeclaration(f, indentDepth)

            # print the wire declarations of the float signals of the "input/output" ports
            if self.portList:
                f.write(curIndent(indentDepth) + '// Bind the instance signals to the port signals\n')
                # Note: only the input signals here, output signals have been declared
                for portItem in self.portList:
                    for sigDir, sigType, sigName in zip(portItem.signalDirectionList, portItem.signalTypeList, portItem.signalInstanceList):
                        if sigDir == 'input':
                            if sigType == 'wire':
                                f.write(curIndent(indentDepth) + 'wire ' + sigName + ';\n')
                            if sigType == 'vector':
                                f.write(curIndent(indentDepth) + 'wire [PACKET_LENGTH-1:0] ' + sigName + ';\n')
                        else:
                            pass
                f.write('\n')
                # bind the "input/output" port signals to the module's port signals
                for portItem in self.portList:
                    for instSigName, portSigName, instSigDirection in zip(portItem.signalInstanceList, portItem.signalList, portItem.signalDirectionList):
                        if instSigDirection == 'input':
                            tmpStr = 'assign ' + instSigName + ' = ' + portSigName + ';'
                        else:
                            tmpStr = 'assign ' + portSigName + ' = ' + instSigName + ';'
                        f.write(curIndent(indentDepth) + tmpStr + '\n')
                f.write('\n')
            # print the instantiations
            for instItem in self.instanceList:
                # if the module is a port, then bypass it
                if instItem.moduleType in ['Inport', 'Outport']:
                    instItem.bypassPrintPortInst(f, indentDepth)
                else:
                    instItem.printInstance(f, indentDepth)

            f.write('endmodule')

            # generate the library description file
            if self.generateLib:
                self.printLibDescription(cntPortList)
            else:
                pass
            
            # generate an instance of the current system
            tmpSysInst = VerilogInstance()
            tmpSysInst.instanceName = copy.deepcopy(self.systemName)
            tmpSysInst.hierarchyNameList = copy.deepcopy(self.hierarchyNameList)
            tmpSysInst.moduleType = copy.deepcopy(self.targetFileName)
            tmpSysInst.parameterList = copy.deepcopy(self.parameterList)
#            for paraItem in tmpSysInst.parameterList:
#                paraItem.parameterName = '$'.join([self.targetFileName,paraItem.parameterName])
            tmpSysInst.portList = copy.deepcopy(self.portList)
            tmpSysInst.parameterAssignmentList = tmpSysInst.parameterList
            
            
            print('Module "' + self.targetFileName + '" is generated.\n')
            return copy.deepcopy(tmpSysInst)

    def printTestbench(self):
        """
        Print the body of the Verilog testbench, including the top system.

        The body includes: module declaration, parameter assignment, stimuli,
        and DUT instantiation.
        Note: The 'instanceList' shoule be well constructed before generating
        the Verilog module, otherwise the information may be wrong.
        """
        # set up the library, mapping rules and the system model data list
        if not self.libraryModuleDict:
            self.constructLibraryList()
            self.constructLibrary()

        if not self.mappingRuleDict:
            self.constructMappingList()
            self.constructMapping()

        if not self.systemList:
            self.constructMdlList()

        # Construct the instance list, recursively
        self.constructInstanceList()

        # first judge the 'targetDirectory'
        if not self.targetDirectory:
            self.targetDirectory = os.getcwd()
        else:
            if not os.path.exists(self.targetDirectory):    # if the target directory is not exist
                os.makedirs(self.targetDirectory)   # create the directory
        self.targetFileName = self.systemName
        self.targetFile = os.path.join(self.targetDirectory, self.targetFileName + '_tb.V')

        # construct the port list of this system
        self.constructSystemParameterList()
        
        with open(self.targetFile, 'w+') as f:
            indentDepth = 0
            # print the header of the module
            self.printHeader(f,indentDepth,self.systemName + '_tb',file_type='testbench')
            # print the precompile directives
            f.write('`ifndef CLK_PERIOD_TOP\n  `define CLK_PERIOD_TOP 2\n`endif\n\n')
            f.write('`timescale 1ns/1ns\n')

            indentDepth += 1
            curIndent = lambda x: ' ' * TAB_SPACE_RATIO * x

            # print the module declaration
            tmpStr = 'module ' + self.systemName + '_tb;'
            f.write(tmpStr + '\n')

            # print the 'include' statement
            f.write(curIndent(indentDepth) + '`include "' + self.libraryModuleDict['GlobalParameterFile'] + '"\n\n')

            # print the parameter declarations
            for instItem in self.instanceList:
                instItem.printParameter(f, indentDepth)

            # print the wire declaration of the interconnects
            for instItem in self.instanceList:
                instItem.printWireDeclaration(f, indentDepth)

            ###############################################################
            # print the probes and stimuli, including:
            #   1, the register for clock and reset;
            #   2, the floating signals of the primitives (data, irdy)
            #   3, the file operator (integer) and wire vectors
            ###############################################################
            # the clock and reset declaration
            f.write(curIndent(indentDepth) + '//###################### probes and stimuli ######################\n')
            f.write(curIndent(indentDepth) + 'reg ' + self.libraryModuleDict['Clock'] + ';\n')
            f.write(curIndent(indentDepth) + 'reg ' + self.libraryModuleDict['Reset'] + ';\n')
            f.write('\n')
            # the registers (stimuli) for floating signals
            # go though each instance, and add the declaration
            floatSignalList = []
            floatSignalTypeList = []
            floatSignalInitList = []    # the list of initial value
            for instItem in self.instanceList:
                for portItem in instItem.portList:
                    if portItem.portStatus == 0:    # floating port
                        for index, wireType in enumerate(portItem.signalTypeList):
                            if portItem.signalDirectionList[index] == 'input':
                                floatSignalList.append(portItem.signalInstanceList[index])
                                floatSignalTypeList.append(wireType)
                                floatSignalInitList.append(portItem.signalFloatValueList[index])
                            else:
                                pass

            for index, signal in enumerate(floatSignalList):
                if floatSignalTypeList[index] == 'wire':
                    f.write(curIndent(indentDepth) + 'reg ' + signal + ';\n')
                if floatSignalTypeList[index] == 'vector':
                    f.write(curIndent(indentDepth) + 'reg [PACKET_LENGTH-1:0] ' + signal + ';\n')
            f.write('\n')

            # generate the clock input
            f.write(curIndent(indentDepth) + 'always #(`CLK_PERIOD_TOP/2) ' + self.libraryModuleDict['Clock'] + ' = ~' + self.libraryModuleDict['Clock'] + ';\n')
            f.write('\n')
            ###############################################################
            # print the testbench (initial module)
            ###############################################################
            f.write(curIndent(indentDepth) + 'initial fork\n')
            indentDepth += 1
            # set the file operator and the monitor
            ##### To be added

            # set the clock and the reset signals
            f.write(curIndent(indentDepth) + '#1 ' + self.libraryModuleDict['Clock'] + ' = 0;\n')
            f.write(curIndent(indentDepth) + '#1 ' + self.libraryModuleDict['Reset'] + ' = 1;\n')
            f.write(curIndent(indentDepth) + '#2 ' + self.libraryModuleDict['Reset'] + ' = 0;\n')
            f.write(curIndent(indentDepth) + '#3 ' + self.libraryModuleDict['Reset'] + ' = 1;\n')
            f.write('\n')
            # set the signal values of the floating signals
            if len(floatSignalList) > 0:
                f.write(curIndent(indentDepth) + '// initialize the floating signals\n')
                for index, signal in enumerate(floatSignalList):
                    f.write(curIndent(indentDepth) + '#1 ' + signal + ' = ' + str(floatSignalInitList[index]) + ';\n')
                f.write('\n')

            indentDepth -= 1
            f.write(curIndent(indentDepth) + 'join\n\n')

            ###############################################################
            # print the instantiations
            ###############################################################
            for instItem in self.instanceList:
                # if the module is a port, then bypass it
                if instItem.moduleType in ['Inport', 'Outport']:
                    instItem.bypassPrintPortInst(f, indentDepth)
                else:
                    instItem.printInstance(f, indentDepth)

            f.write('endmodule')
