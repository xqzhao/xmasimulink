# Copyright (C) 2014 Xueqian Zhao.  All rights reserved.
# ----------------------------------------------------------------------
# FILE NAME : xmas_verilog.vlib
# DEPARTMENT : ES/ICT/KTH Royal Institute of Technology
# AUTHOR: Xueqian Zhao
# AUTHOR��S EMAIL : xueq@kth.se
# --------------------------------------------------------------------
# RELEASE HISTORY
# VERSION DATE AUTHORDESCRIPTION
# 1.0 2014/6/12 Xueqian Zhao  created
# DESCRIPTION
# This file discribes the xMAS library implemented by Verilog HDL in terms of
# the interfaces and the parameters for modules including:
#   global signals and setups, queue_primitive, regulated_source_rp, 
#   sink_primitive, fork_primitive, join_primitive, merge_primitive,
#   switch_primitive, service_curve_rp, packet_trans, credit_counter
# ---------------------------------------------------------------------
# FORMAT is as below:
#Module {
#  Name    module_name
#  Ports   [numberOfInput, numberOfOutput]
#  # list each input ports
#  Inport {
#    PortID  1
#    #input and output signals and output signals of a port
#    Input   ["wire",   "irdy", "i_irdy_fifo"]
#    Input   ["vector", "data", "iv_data_fifo"]
#    Output  ["wire",   "trdy", "o_trdy_fifo"]
#  }
#  # list each output ports
#  Outport {
#    PortID  1
#    Input   ["wire",   "trdy", "i_trdy_fifo"]
#    Output  ["vector", "data", "ov_data_fifo"]
#    Output  ["wire",   "irdy", "o_irdy_fifo"]
#  }
#  # list all the parameters in the module
#  Parameter {
#    # parameterName [dataType, defaultValue]
#    FIFO_CAPACITY   ["integer", 5]
#    POINTER_WIDTH   ["integer", 4]
#  }
#}


#'Global' defines to the global signals and parameters
Global {
  Reset   "i_reset_n"
  Clock   "i_clk"
  GlobalParameterFile "global_parameter.v"
}

Module {
  Name    "queue_primitive"
  Ports   [1,1]
  Inport {
    PortID  1
    Input   ["wire",   "irdy", "i_irdy_fifo"]
    Input   ["vector", "data", "iv_data_fifo"]
    Output  ["wire",   "trdy", "o_trdy_fifo"]
  }
  Outport {
    PortID  1
    Input   ["wire",   "trdy", "i_trdy_fifo"]
    Output  ["vector", "data", "ov_data_fifo"]
    Output  ["wire",   "irdy", "o_irdy_fifo"]
  }
  Parameter {
    FIFO_CAPACITY   ["integer", 5]
    POINTER_WIDTH   ["integer", 4]
  }
}

Module {
  Name    "regulated_source_rp"
  Ports   [0,1]
  Outport {
    PortID  1
    Input   ["wire",   "trdy", "i_trdy_regulated_source"]
    Output  ["vector", "data", "ov_data_regulated_source"]
    Output  ["wire",   "irdy", "o_irdy_regulated_source"]
  }
  Parameter {
    FLOW_ID       ["integer", 0]
    PACKET_TYPE   ["string", "REQUEST"]
    BURST         ["integer", 4]
    RATE          ["real", 0.1]
    INJECTION_BUFFER_SIZE   ["integer", 500]
  }
}

Module {
  Name    "sink_primitive"
  Ports   [1,0]
  Inport {
    PortID  1
    Input   ["wire",   "irdy", "i_irdy_sink"]
    Input   ["vector", "data", "iv_data_sink"]
    Output  ["wire",   "trdy", "o_trdy_sink"]
  }
}

Module {
  Name    "fork_primitive"
  Ports   [1,2]
  Inport {
    PortID  1
    Input   ["wire",   "irdy", "i_irdy_fork"]
    Input   ["vector", "data", "iv_data_fork"]
    Output  ["wire",   "trdy", "o_trdy_fork"]
  }
  Outport {
    PortID  1
    Input   ["wire",   "trdy", "i_trdy_fork_a"]
    Output  ["vector", "data", "ov_data_fork_a"]
    Output  ["wire",   "irdy", "o_irdy_fork_a"]
  }
  Outport {
    PortID  2
    Input   ["wire",   "trdy", "i_trdy_fork_b"]
    Output  ["vector", "data", "ov_data_fork_b"]
    Output  ["wire",   "irdy", "o_irdy_fork_b"]
  }
  Parameter {
    A_TYPE    ["string", "REQUEST"]
    B_TYPE    ["string", "REQUEST"]
  }
}

Module {
  Name    "join_primitive"
  Ports   [2,1]
  Inport {
    PortID  1
    Input   ["wire",   "irdy", "i_irdy_join_a"] 
    Input   ["vector", "data", "iv_data_join_a"] 
    Output  ["wire",   "trdy", "o_trdy_join_a"]
  }
  Inport {
    PortID  2
    Input   ["wire",   "irdy", "i_irdy_join_b"]
    Input   ["vector", "data", "iv_data_join_b"]
    Output  ["wire",   "trdy", "o_trdy_join_b"]
  }
  Outport {
    PortID  1
    Input   ["wire",   "trdy", "i_trdy_join"]
    Output  ["vector", "data", "ov_data_join"]
    Output  ["wire",   "irdy", "o_irdy_join"]
  }
}

Module {
  Name    "merge_primitive"
  Ports   [2,1]
  Inport {
    PortID  1
    Input   ["wire",   "irdy", "i_irdy_merge_a"]
    Input   ["vector", "data", "iv_data_merge_a"]
    Output  ["wire",   "trdy", "o_trdy_merge_a"]
  }
  Inport {
    PortID  2
    Input   ["wire",   "irdy", "i_irdy_merge_b"]
    Input   ["vector", "data", "iv_data_merge_b"]
    Output  ["wire",   "trdy", "o_trdy_merge_b"]
  }
  Outport {
    PortID  1
    Input   ["wire",   "trdy", "i_trdy_merge"]
    Output  ["vector", "data", "ov_data_merge"]
    Output  ["wire",   "irdy", "o_irdy_merge"]
  }
  Parameter {
    WEIGHT_A    ["integer", 1]
    WEIGHT_B    ["integer", 1]
  }
}

# the 3-port merge
Module {
  Name    "merge3_primitive"
  Ports   [3,1]
  Inport {
    PortID  1
    Input   ["wire",   "irdy", "i_irdy_merge_a"]
    Input   ["vector", "data", "iv_data_merge_a"]
    Output  ["wire",   "trdy", "o_trdy_merge_a"]
  }
  Inport {
    PortID  2
    Input   ["wire",   "irdy", "i_irdy_merge_b"]
    Input   ["vector", "data", "iv_data_merge_b"]
    Output  ["wire",   "trdy", "o_trdy_merge_b"]
  }
  Inport {
    PortID  3
    Input   ["wire",   "irdy", "i_irdy_merge_c"]
    Input   ["vector", "data", "iv_data_merge_c"]
    Output  ["wire",   "trdy", "o_trdy_merge_c"]
  }
  Outport {
    PortID  1
    Input   ["wire",   "trdy", "i_trdy_merge"]
    Output  ["vector", "data", "ov_data_merge"]
    Output  ["wire",   "irdy", "o_irdy_merge"]
  }
  Parameter {
    WEIGHT_A    ["integer", 1]
    WEIGHT_B    ["integer", 1]
    WEIGHT_C    ["integer", 1]
  }
}

# the 4-port merge
Module {
  Name    "merge4_primitive"
  Ports   [4,1]
  Inport {
    PortID  1
    Input   ["wire",   "irdy", "i_irdy_merge_a"]
    Input   ["vector", "data", "iv_data_merge_a"]
    Output  ["wire",   "trdy", "o_trdy_merge_a"]
  }
  Inport {
    PortID  2
    Input   ["wire",   "irdy", "i_irdy_merge_b"]
    Input   ["vector", "data", "iv_data_merge_b"]
    Output  ["wire",   "trdy", "o_trdy_merge_b"]
  }
  Inport {
    PortID  3
    Input   ["wire",   "irdy", "i_irdy_merge_c"]
    Input   ["vector", "data", "iv_data_merge_c"]
    Output  ["wire",   "trdy", "o_trdy_merge_c"]
  }
  Inport {
    PortID  4
    Input   ["wire",   "irdy", "i_irdy_merge_d"]
    Input   ["vector", "data", "iv_data_merge_d"]
    Output  ["wire",   "trdy", "o_trdy_merge_d"]
  }
  Outport {
    PortID  1
    Input   ["wire",   "trdy", "i_trdy_merge"]
    Output  ["vector", "data", "ov_data_merge"]
    Output  ["wire",   "irdy", "o_irdy_merge"]
  }
  Parameter {
    WEIGHT_A    ["integer", 1]
    WEIGHT_B    ["integer", 1]
    WEIGHT_C    ["integer", 1]
    WEIGHT_D    ["integer", 1]
  }
}

Module {
  Name    "switch_primitive"
  Ports   [1,2]
  Inport {
    PortID  1
    Input   ["wire",   "irdy", "i_irdy_switch"]
    Input   ["vector", "data", "iv_data_switch"]
    Output  ["wire",   "trdy", "o_trdy_switch"]
  }
  Outport {
    PortID  1
    Input   ["wire",   "trdy", "i_trdy_switch_a"]
    Output  ["vector", "data", "ov_data_switch_a"]
    Output  ["wire",   "irdy", "o_irdy_switch_a"]
  }
  Outport {
    PortID  2
    Input   ["wire",   "trdy", "i_trdy_switch_b"]
    Output  ["vector", "data", "ov_data_switch_b"]
    Output  ["wire",   "irdy", "o_irdy_switch_b"]
  }
  Parameter {
    FLOW_ID_TARGET       ["integer", 1]
  }
}

Module {
  Name    "switch3_primitive"
  Ports   [1,3]
  Inport {
    PortID  1
    Input   ["wire",   "irdy", "i_irdy_switch"]
    Input   ["vector", "data", "iv_data_switch"]
    Output  ["wire",   "trdy", "o_trdy_switch"]
  }
  Outport {
    PortID  1
    Input   ["wire",   "trdy", "i_trdy_switch_a"]
    Output  ["vector", "data", "ov_data_switch_a"]
    Output  ["wire",   "irdy", "o_irdy_switch_a"]
  }
  Outport {
    PortID  2
    Input   ["wire",   "trdy", "i_trdy_switch_b"]
    Output  ["vector", "data", "ov_data_switch_b"]
    Output  ["wire",   "irdy", "o_irdy_switch_b"]
  }
  Outport {
    PortID  3
    Input   ["wire",   "trdy", "i_trdy_switch_c"]
    Output  ["vector", "data", "ov_data_switch_c"]
    Output  ["wire",   "irdy", "o_irdy_switch_c"]
  }
  Parameter {
    FLOW_ID_TARGET       ["integer", 1]
  }
}

Module {
  Name    "service_curve_rp"
  Ports   [1,1]
  Inport {
    PortID  1
    Input   ["wire",   "irdy", "i_irdy_service"]
    Input   ["vector", "data", "iv_data_service"]
    Output  ["wire",   "trdy", "o_trdy_service"]
  }
  Outport {
    PortID  1
    Input   ["wire",   "trdy", "i_trdy_service"]
    Output  ["vector", "data", "ov_data_service"]
    Output  ["wire",   "irdy", "o_irdy_service"]
  }
  Parameter {
    LATENCY       ["integer", 10]
    RATE          ["real", 0.9]
  }
}

Module {
  Name    "packet_trans"
  Ports   [1,1]
  Inport {
    PortID  1
    Input   ["wire",   "irdy", "i_irdy_func"]
    Input   ["vector", "data", "iv_data_func"]
    Output  ["wire",   "trdy", "o_trdy_func"]
  }
  Outport {
    PortID  1
    Input   ["wire",   "trdy", "i_trdy_func"]
    Output  ["vector", "data", "ov_data_func"]
    Output  ["wire",   "irdy", "o_irdy_func"]
  }
}

Module {
  Name    "credit_counter"
  Ports   [1,1]
  Inport {
    PortID  1
    Input   ["wire",   "irdy", "i_irdy_credit"]
    Input   ["vector", "data", "iv_data_credit"]
    Output  ["wire",   "trdy", "o_trdy_credit"]
  }
  Outport {
    PortID  1
    Input   ["wire",   "trdy", "i_trdy_credit"]
    Output  ["vector", "data", "ov_data_credit"]
    Output  ["wire",   "irdy", "o_irdy_credit"]
  }
  Parameter {
    COUNTER_SIZE       ["integer", 5]
    QUEUE_POINTER_WIDTH          ["integer", 4]
  }
}
